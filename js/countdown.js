function countdown(startHour, startMinute, redirectTarget) {
    const countdownElement = document.getElementById('countdown');
    const preCountdownElement = document.getElementById('preCountdown');
    const formElement = document.getElementById('form');
    const announcementElement = document.getElementById('announcement');
    const h1Element = document.querySelector('h1');
    const h2Element = document.querySelector('h2');
    const logoElement = document.getElementById('logo');
    const footerElement = document.getElementsByTagName('footer')[0];
    const headerElement = document.getElementsByTagName('header')[0];
    const artElement = document.getElementById('art');
    const overlayElement = document.getElementById('overlay');
    const mainElement = document.getElementsByTagName('main')[0];
    const readyElement = document.getElementById('ready');
    const endOfTimeElement = document.getElementById('endOfTime');
    const timeOverElement = document.getElementById('timeOver');
    const countdownWrapperElement = document.getElementById('countdownWrapper');
    const feedbackElement = document.getElementById('feedback');
    const feedbackFormElement = document.getElementById('feedbackForm');
    const feedbackButtonElement = document.getElementById('feedbackButton');

    const now = new Date();
    const getTime = (hour, minute) => {
        return new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour, minute);
    }

    const formatTime = number => number.toString().padStart(2, '0');

    const getTimeUnits = diff => `${formatTime(Math.floor((diff / 1000 / 60) % 60))}:${formatTime(Math.floor((diff / 1000) % 60))}`;

    const startTime = getTime(startHour, startMinute);

    let endHour = startHour;
    let endMinute = startMinute + chapterDuration;
    endHour += Math.floor(endMinute / 60);
    endMinute %= 60;
    endHour %= 24;

    const endTime = getTime(endHour, endMinute);

    let state = {
        preCountdown: '',
        countdown: '',
        formDisplay: 'none'
    };

    const updateElementAndState = (element, stateKey, newValue) => {
        if (newValue !== state[stateKey]) {
            element.textContent = newValue;
            state[stateKey] = newValue;
        }
    }

    function updateCountdown() {
        const now = new Date();
        const preDiff = startTime - now;
        const diff = endTime - now;

        updateElementAndState(preCountdownElement, 'preCountdown', (now >= startTime) ? '' : getTimeUnits(preDiff));
        updateElementAndState(countdownElement, 'countdown', (now < startTime || now >= endTime) ? '' : getTimeUnits(diff));

        const newFormDisplay = (now < startTime || now >= endTime) ? 'none' : 'flex';
        if (newFormDisplay !== state.formDisplay) {
            formElement.style.display = newFormDisplay;
            state.formDisplay = newFormDisplay;
        }

        if (preDiff <= 60000) {
            artElement.style.display = 'none';
            mainElement.style.justifyContent = 'center';
            headerElement.style.alignItems = 'center';
            document.getElementById('decor4').style.display = 'none';
        }

        if (preDiff <= 49000 && preDiff > 30000) {
            readyElement.style.display = 'flex';
            setTimeout(() => readyElement.style.opacity = '1', 100);
        } else if (preDiff <= 38000) {
            readyElement.style.opacity = '0';
        }

        if (preDiff <= 9000) {
            readyElement.style.cssText = "opacity: 1; color: #85CB33; display: flex";
            readyElement.textContent = 'استعينوا بالله';
        }

        if (diff <= 30000 && diff > 0) {
            overlayElement.style.display = 'flex'
            countdownElement.style.cssText = 'font-size: 2em; color: #FB3640;';
            countdownWrapperElement.style.backgroundColor = 'transparent'
            setTimeout(function () {
                overlayElement.style.opacity = '1';
            }, 100);
        } else {
            overlayElement.style.opacity = '0';
        };


        if (now - startTime >= 30000) {
            h1Element.style.display = 'none';
            h2Element.style.display = 'none';
            announcementElement.style.display = 'none';
            formElement.style.height = '90%'
        }

        if (now >= startTime) {
            announcementElement.textContent = 'الوقت المتبقي';
            h1Element.style.fontSize = '1.5em';
            h2Element.style.fontSize = '0.8em';
            logoElement.style.height = '2em';
            footerElement.style.position = 'relative';
            artElement.style.display = 'none';
            mainElement.style.justifyContent = 'center';
            headerElement.style.alignItems = 'center';
            readyElement.style.display = 'none';
        };

        if (now >= endTime) {
            endOfTimeElement.style.display = "flex";
            countdownWrapperElement.style.display = "none";
            h1Element.style.display = "none";
            h2Element.style.display = "none";
            announcementElement.style.display = "none";
            footerElement.style.display = "none";
            document.querySelector('#decor img').style.cssText = '';

            if (redirectTarget) {
                setTimeout(() => window.location.href = redirectTarget, 3000);
            } else {
                timeOverElement.textContent = 'بالتوفيق والنجاح';
                timeOverElement.style.fontSize = 'clamp(1em, 7vw, 4em)';
                h1Element.style.display = "flex";
                h2Element.style.display = "flex";
                footerElement.style.display = "flex";
                document.body.style.backgroundImage = "linear-gradient(30deg, #05132e, #3968c0)";
                feedbackElement.style.display = flex;
                setTimeout(() => {
                    feedbackElement.style.opacity = 1;
                }, 2000);
            }
        } else {
            setTimeout(updateCountdown, 1000);
        }
    }

    updateCountdown();
}

const startExam = "23:40";
const chapterDuration = 5;
const chapterGap = chapterDuration + 0;
let [startHour, startMinute] = startExam.split(':').map(Number);

const urlIndex = Number(document.querySelector('#form').dataset.urlIndex);

const totalMinutes = startHour * 60 + startMinute + urlIndex * chapterGap;
[startHour, startMinute] = [Math.floor(totalMinutes / 60), totalMinutes % 60];

let chapterFile = null;

if (urlIndex !== 5) {
    chapterFile = `Chapter${urlIndex + 2}.html`;
}

countdown(startHour, startMinute, chapterFile)


